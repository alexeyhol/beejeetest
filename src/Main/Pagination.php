<?php
/*Основной класс вывода данных с пагинацией*/
namespace App\Main;

use PDO;

class Pagination
{
    private $pdo;
    
    private $num_page;
    private $per_page;
    private $total;
    
    
    public function __construct($pdo, $per_page)
    {
        $this->pdo = $pdo;
        $this->per_page = $per_page;
        
        $rs = $this->pdo->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM `taskbook`");
        $rs->execute();
        $rs = $this->pdo->prepare("SELECT FOUND_ROWS()");
        $rs->execute();
        
        $this->total = $rs->fetch(\PDO::FETCH_COLUMN);
        $this->num_page = ceil($this->total / $this->per_page);
    }
    
    public function getTasks($sort = 'normal')
    {
        $current_page = 1;
        
        if (isset($_GET['page']) && $_GET['page'] > 0) {
            $current_page = $_GET['page'];
        }
        
        $start = ($current_page - 1) * $this->per_page;
        
        $query = null;
        $queryMain = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` LIMIT ?,?";
        $querySortByEmail = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `email` ASC LIMIT ?,?";
        $querySortByName = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `name` ASC LIMIT ?,?";
        $querySortByStatus = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `status` ASC LIMIT ?,?";
        
        switch ($sort) {
            case 'normal': $query = $queryMain;
                break;
            case 'email': $query = $querySortByEmail;
                break;
            case 'name': $query = $querySortByName;
                break;
            case 'done': $query = $querySortByStatus;
                break;
            default: $query = $queryMain;
                break;
        }
       
        $stmt = $this->pdo->prepare($query);
        $stmt ->execute([$start, $this->per_page]);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
        $page = 0;
       
        if (isset($_COOKIE['admin'])) {
            foreach ($data as $value) {
                echo <<<EOL
        
        <b>Name</b>: {$value['name']}<br>
        <b>Email</b>: {$value['email']}<br>
        <form action="src/handlers/adminActionHandler.php" method="POST">
        <input name="id" type="hidden" value={$value['id']}>
        <textarea name="task" cols="50" rows="10" >{$value['task']}</textarea><br>

        <label>DONE:
        <select name="status">
            <option value="0">NO</option>
            <option value="1">YES</option>
        </select>
        </label>

        <button type="submit">Change</button>
        </form>
        <hr>
EOL;
            }
        } else {
            foreach ($data as $value) {
                $status = 0;
                if ($value['status'] == 0) {
                    $status = 'NO';
                } else {
                    $status = 'YES';
                }
                echo <<<EOL
        <b>Name</b>: {$value['name']}<br>
        <b>Email</b>: {$value['email']}<br>
        <b>Task</b>: {$value['task']}<br>
        <b>Done</b>: {$status}<br>
        <hr>
EOL;
            }
        }
    
        while ($page++ < $this->num_page) {
            if ($page == $current_page) {
                echo "<b>{$page}</b>";
            } else {
                echo "<a class=\"page\" href=\"?page={$page}\">{$page}</a>";
            }
        }
    }
}
