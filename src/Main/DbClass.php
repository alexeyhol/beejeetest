<?php

namespace App\Main;

use \PDO;

/**
 * Creating a database connection
 */
class DbClass
{
    protected $connection;
    
    private const OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
    ];
    
    public function __construct($dbdriver, $host, $dbname, $charset, $username, $passwd)
    {
        $dsn = "$dbdriver:host=$host;dbname=$dbname;charset=$charset";
        $this->connection = new PDO($dsn, $username, $passwd, self::OPTIONS);
    }
    
    public function getConnection()
    {
        return $this->connection;
    }

    /* Добавление данных в базу (сообщения) */

    public function addTask($name, $email, $task)
    {
        $query = "INSERT INTO `taskbook`(`name`, `email`, `task`) VALUES (?, ?, ?)";
        $stmt = $this->connection->prepare($query);
        $stmt ->execute([$name, $email, $task]);
    }

    /* Получение данных из базы (сообщения) */
    
    public function getTask($sort = 'normal')
    {
        $query = null;
        $queryMain = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook`";
        $querySortByEmail = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `email` ASC";
        $querySortByName = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `name` ASC";
        $querySortByStatus = "SELECT `id`, `name`, `email`, `task`, `status` FROM `taskbook` ORDER BY `status` ASC";
        
        switch ($sort) {
            case 'normal': $query = $queryMain;
                break;
            case 'email': $query = $querySortByEmail;
                break;
            case 'name': $query = $querySortByName;
                break;
            case 'done': $query = $querySortByStatus;
                break;
            default: $query = $queryMain;
                break;
        }
        
        $stmt = $this->connection->prepare($query);
        $stmt ->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}
