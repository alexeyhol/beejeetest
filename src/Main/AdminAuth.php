<?php

namespace App\Main;

class AdminAuth
{
    private $admin_name;
    private $passwd;
    
    public function __construct()
    {
        $this->admin_name = $_POST['admin_name'];
        $this->passwd =(int)$_POST['passwd'];
    }
    
    public function isAdmin($pdo)
    {
        $isAdmin = false;
        
        $query = "SELECT `name`, `passwd` FROM `admin`";
        $stmt = $pdo->prepare($query);
        $stmt ->execute();
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        $correct_admin_name = $data['name'];
        $correct_admin_passwd = $data['passwd'];
        
        if ($correct_admin_name === $this->admin_name && $correct_admin_passwd === $this->passwd) {
            $isAdmin = true;
        }
        return $isAdmin;
    }
    
    public function setAdmin($isAdmin)
    {
        if ($isAdmin) {
            setcookie('admin', 'admin', 0, '/');
            header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}");
        } else {
            echo '<div style="text-align: center">';
            echo "<h2>Invalid username and / or password. Repeat entry!</h2>";
            echo '<hr style="width: 20%">';
            echo "<strong><a href=\"{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}\">REPEAT ENTRY</a></strong>";
            echo "</div>";
        }
    }
}
