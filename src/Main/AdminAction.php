<?php
/*Действия администратора по изменению задач*/
namespace App\Main;

use \PDO;

class AdminAction extends \PDO
{
    private $id;
    private $task;
    private $status;
    
    public function __construct()
    {
        $this->id = $_POST['id'];
        $this->task = htmlspecialchars($_POST['task']);
        $this->status = (int)$_POST['status'];
    }
    
   
    public function getId()
    {
        return $this->id;
    }
    
    public function getTask()
    {
        return $this->task;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    
    public function changeStatus($pdo, $status, $id)
    {
        $query = "UPDATE `taskbook` SET `status` = ? WHERE `id` = ?";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$status, $id]);
    }
    
    public function changeTask($pdo, $task, $id)
    {
        $query = "UPDATE `taskbook` SET `task` = ? WHERE `id` = ?";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$task, $id]);
    }
}
