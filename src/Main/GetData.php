<?php

namespace App\Main;

/**
 * get data from $_POST
 */
class GetData
{
    private $allData;
    
    private $name;
    private $email;
    private $task;
    
    public function __construct()
    {
        $filter = [
            'name' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_EMAIL,
            'task' => FILTER_SANITIZE_FULL_SPECIAL_CHARS
        ];
        
        $this->allData = filter_input_array(INPUT_POST, $filter);
        
        foreach ($this->allData as $key => $value) {
            switch ($key) {
                case 'name': $this->name = $value;
                    break;
                case 'email': $this->email = $value;
                    break;
                case 'task': $this->task = $value;
            }
        }
    }
    
    public function getAll()
    {
        return $this->allData;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getTask()
    {
        return $this->task;
    }
}
