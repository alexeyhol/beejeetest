<?php
/*Обработчик автризации админа*/
require_once '../../vendor/autoload.php';

$connection = new App\Main\DbClass(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$login = new App\Main\AdminAuth();

$isAdmin = $login->isAdmin($connection->getConnection());

$login->setAdmin($isAdmin);
