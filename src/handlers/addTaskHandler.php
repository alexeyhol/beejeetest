<?php
/*Обработчик на добавление данных в базу*/
require_once '../../vendor/autoload.php';

$connection = new App\Main\DbClass(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$data = new App\Main\GetData($_POST);

$connection->addTask($data->getName(), $data->getEmail(), $data->getTask());

header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}");
