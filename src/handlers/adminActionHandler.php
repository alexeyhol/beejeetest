<?php
/*Обработчик действий админа*/
require_once '../../vendor/autoload.php';

$connection = new App\Main\DbClass(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$pdo = $connection->getConnection();

$change = new \App\Main\AdminAction();

$change->changeStatus($pdo, $change->getStatus(), $change->getId());
$change->changeTask($pdo, $change->getTask(), $change->getId());

header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}");
