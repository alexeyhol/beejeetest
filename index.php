<?php

require_once './vendor/autoload.php';

$connection = new App\Main\DbClass(DB_DRIVER, HOST, DB_NAME, CHARSET, USERNAME, PASSWD);

$param = new \App\Main\SortData();

$pag = new \App\Main\Pagination($connection->getConnection(), 3);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Book</title>
    <link rel="stylesheet" href="assets/styles/main.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="u-pull-right">
                <?php
                if (isset($_COOKIE['admin'])) {
                    echo '<b>Hello, admin</b> ';
                } else {
                    echo <<<EOL
                    <form action="src/handlers/adminAuthHandler.php" method="post" id="login">
                        <input type="text" name="admin_name" placeholder="name">
                        <input type="password" name="passwd" placeholder="password">
                        <input type="submit" name="login" value="LOGIN">
                    </form> 
EOL;
                }
                ?>
            </div>
            
            <div class="twelve columns">
                <h1 class="header">Task Book</h1>
                <h2>Add Task</h2>
                
                <form action="src/handlers/addTaskHandler.php" method="post" id="add_task">
                    <input type="text" name="name" placeholder="Enter your name"><br>
                    <input type="email" name="email" placeholder="Enter your email"><br>
                    <textarea name="task" cols="30" rows="10" placeholder="Add task"></textarea><br>
                    <button type="submit" form="add_task">Add</button>
                </form>
            </div>
        </div>
        
        <div class="row">
            
            <hr>
            
            <h2>Task List</h2>
            
            <form id="sortByName" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" >
                <input type="hidden" name="sort" value="name">
            </form>
            
            <form id="sortByEmail" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" >
                <input type="hidden" name="sort" value="email">
            </form>
            
            <form id="sortByStatus" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" >
                <input type="hidden" value="sort_by_status">
            </form>
       
            <form id="adminAction" action="src/handlers/adminActionHandler.php" method="POST"></form>
            <form id="adminLoggout" action="src/handlers/adminActionHandler.php" method="POST"></form>

            <button type="submit" form="sortByName">Sort by name</button>
            <button type="submit" form="sortByEmail">Sort by email</button>
            <button type="submit" form="sortByStatus">Sort by status</button>
            
            <div class="twelve columns">
                <?php
                    $pag->getTasks($param->getSortParam());
                ?>
                
            </div> 
        </div>
    </div>

    <footer>
        
    </footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>

